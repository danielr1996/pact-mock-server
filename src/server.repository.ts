import * as path from "path";
import * as fs from 'fs';
import * as os from 'os';
import {PortUtil} from "./port.util";
import environment from "./environment";

const pact = require('@pact-foundation/pact-node');

export class ServerRepository {
    private servers: MockServer[] = [];

    getAllServer(): MockServer[] {
        return this.servers;
    }

    getServerByPort(port: number): MockServer {
        return this.servers.filter(server => {
            return server.port === port
        })[0];

    }

    addServer(server: MockServer): number {
        this.servers.push(server);
        return server.port;
    }

    updateServer(port: number, server: MockServer): void {

        let index = this.servers.findIndex(server=>server.port === port);
        this.servers[index] = server;
    }

    deleteServer(port: number): void {
        console.log("Delete Server");
        let index = this.servers.findIndex(server=>server.port === port);
        this.servers.splice(index, 1);
    }
}

export class MockServer {
    constructor(
        public port: number,
        public name: string = '',
        public pact: string = '',
        public server: any = null,
    ) {
    }
}

export function mockServerFactory(rawPactFile: string, runningServers: MockServer[]): MockServer {
    const jsonPactFile = JSON.parse(rawPactFile);
    const name = jsonPactFile['consumer']['name'] + '-' + jsonPactFile['provider']['name'] + '.json';
    fs.writeFileSync(`${os.tmpdir()}${path.sep}${name}`, rawPactFile);
    let port: number = PortUtil.getFreePort(runningServers, environment.upperPort, environment.lowerPort);
    const server = pact.createStub({
        cors: true,
        port: port,
        host: '0.0.0.0',
        pactUrls: [`${os.tmpdir()}${path.sep}${name}`]
    });
    pact.logLevel('debug');
    return new MockServer(port, name, rawPactFile, server);
}