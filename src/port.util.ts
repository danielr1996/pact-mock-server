import {MockServer} from "./server.repository";

export class PortUtil {
    static getFreePort(servers: MockServer[], upper, lower) {
        var diff = upper - lower;
        var counter = 0;
        var desiredPort;

        do {
            desiredPort = this.generateRandomInteger(upper, lower);
            counter++;
            if (counter > diff) {
                return false;
            }
        } while (!this.portAvailable(servers, desiredPort));
        return desiredPort;
    }

    static generateRandomInteger(min, max) {
        return Math.floor(min + Math.random() * (max + 1 - min))
    }

    static portAvailable(servers: MockServer[], port) {
        var available = true;
        servers.forEach((function (server: MockServer) {
            if (server.port === port) {
                available = false;
            }
        }));
        return available;
    }
}