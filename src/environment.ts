import * as process from "process";

const environment = {
    upperPort: process.env['upper'] || 30010,
    lowerPort: process.env['upper'] || 30000
};

export default environment;