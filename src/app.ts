import * as express from 'express'
import {MockServer, mockServerFactory, ServerRepository} from "./server.repository";
import * as path from "path";

var cors = require('cors');

const bodyParser = require('body-parser');

class App {
    public express;
    private repo: ServerRepository = new ServerRepository();

    constructor() {
        this.express = express();
        this.mountRoutes()
    }

    private mountRoutes(): void {
        const router = express.Router();
        router.get('/server', (req, res) => {
            res.send(this.repo.getAllServer().map((server: MockServer) => {
                return {
                    name: server.name,
                    port: server.port,
                    pact: server.pact
                }
            }));
        });

        //Get Server by port
        router.get('/server/:port', (req, res) => {
            const server: MockServer = this.repo.getServerByPort(parseInt(req.params['port']));
            res.send({
                name: server.name,
                port: server.port,
                pact: server.pact
            });
        });

        //Add Server
        router.post('/server/', (req, res) => {
            const server: MockServer = mockServerFactory(JSON.stringify(req.body), this.repo.getAllServer());
            const port = this.repo.addServer(server);
            server.server.start();
            res.set('Location', port + '');
            res.set('Access-Control-Expose-Headers', 'Location');
            res.status(201).send();
        });

        //Update Server
        router.put('/server/:port', (req, res) => {
            console.log(req.body);
            console.log(typeof req.body);
            const server: MockServer = mockServerFactory(JSON.stringify(req.body), this.repo.getAllServer());
            this.repo.updateServer(parseInt(req.params['port']), server);
            server.server.start();

            res.sendStatus(204);
        });

        //Delete Server
        router.delete('/server/:port', (req, res) => {
            this.repo.deleteServer(parseInt(req.params['port']));
            res.sendStatus(204);
        });
        this.express.use(bodyParser.json());
        this.express.use(cors());
        this.express.use('/api', router)
        this.express.use('/app', express.static(`${__dirname}${path.sep}..${path.sep}pact-mock-ng${path.sep}dist`, {
            index: "index.html"
        }));
    }
}

export default new App().express