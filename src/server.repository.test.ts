import {MockServer, mockServerFactory, ServerRepository} from "./server.repository";
import {PortUtil} from "./port.util";
import environment from "./environment";


let rawPact = {
    pact: `
    {
  "consumer": {
    "name": "dms-ui"
  },
  "provider": {
    "name": "dms-service"
  },
  "interactions": [
    {
      "description": "a request to GET a document",
      "request": {
        "method": "GET",
        "path": "/documents/1"
      },
      "response": {
        "status": 200,
        "headers": {
        },
        "body": {
          "name": "Rechnung",
          "path": "/Rechnung",
          "tags": [
            "Auto",
            "Rechnung"
          ]
        },
        "matchingRules": {
          "$.body": {
            "match": "type"
          }
        }
      }
    }
  ],
  "metadata": {
    "pactSpecification": {
      "version": "2.0.0"
    }
  }
}`
};

export function portUtilTest() {
    //console.log(PortUtil.getFreePort([], environment.upperPort, environment.lowerPort));
}

export function factoryTest() {
    const mockServer: MockServer = mockServerFactory(rawPact.pact, []);
    mockServer.server.start().then(res => {
        //console.log("Res: "+res)
    });
}