import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MockServer, PactService} from "../pact.service";
import {map, tap} from "rxjs/operators";

@Component({
  selector: 'app-pact-master',
  templateUrl: './pact-master.component.html',
  styleUrls: ['./pact-master.component.scss']
})
export class PactMasterComponent implements OnInit {
  mockservers: MockServer[] = [];

  constructor(private service: PactService) {
  }

  ngOnInit() {
    this.service.getAllPacts().pipe(
      tap(servers => {
        this.mockservers = servers
        servers.forEach(server=>server.pact = JSON.stringify(JSON.parse(server.pact), null, 4))
      }),
    ).subscribe();
  }

  delete(port: number){
   this.service.deletePact(port).subscribe();
  }

  update(port: number, server: MockServer){
    this.service.updatePact(port, server).subscribe();
  }
}
