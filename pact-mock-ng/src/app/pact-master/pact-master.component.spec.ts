import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PactMasterComponent } from './pact-master.component';

describe('PactMasterComponent', () => {
  let component: PactMasterComponent;
  let fixture: ComponentFixture<PactMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PactMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PactMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
