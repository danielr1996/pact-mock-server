import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPactComponent } from './add-pact.component';

describe('AddPactComponent', () => {
  let component: AddPactComponent;
  let fixture: ComponentFixture<AddPactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
