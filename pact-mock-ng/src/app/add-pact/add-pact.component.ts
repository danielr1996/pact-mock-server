import { Component, OnInit } from '@angular/core';
import {PactService} from "../pact.service";

@Component({
  selector: 'app-add-pact',
  templateUrl: './add-pact.component.html',
  styleUrls: ['./add-pact.component.scss']
})
export class AddPactComponent implements OnInit {
  rawPact: string = '';

  constructor(private service: PactService) { }

  ngOnInit() {
  }

  startServer(){
    this.service.addPact(this.rawPact).subscribe(id=>{
      //console.log("ID: "+id)
    });
  }
}
