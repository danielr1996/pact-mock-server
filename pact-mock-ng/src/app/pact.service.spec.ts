import { TestBed, inject } from '@angular/core/testing';

import { PactService } from './pact.service';

describe('PactService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PactService]
    });
  });

  it('should be created', inject([PactService], (service: PactService) => {
    expect(service).toBeTruthy();
  }));
});
