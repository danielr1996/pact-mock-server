import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {environment} from "../environments/environment";
import {map, flatMap, tap} from "rxjs/operators";
import "rxjs/add/observable/of";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class PactService {
  private servers: MockServer[] = [];
  private serverSubject: BehaviorSubject<MockServer[]> = new BehaviorSubject<MockServer[]>(this.servers);

  constructor(private http: HttpClient) {
  }

  getAllPacts(): Observable<MockServer[]> {

    return this.http
      .get<MockServer[]>(`${environment.apihost}/server`)
      .pipe(flatMap(servers=>{
        this.servers = servers;
        this.serverSubject.next((this.servers));
        return this.serverSubject.asObservable();
      }));
  }

  getPactbyPort(port: number): Observable<MockServer> {
    return this.http.get<MockServer>(`${environment.apihost}/server/${port}`);
  }

  addPact(rawPact: string): Observable<number> {
    const options = {
      observe: 'response' as 'response',
      headers: {
        'Content-Type': 'application/json'
      }
    };

    return this.http
      .post<string>(`${environment.apihost}/server`, rawPact, options)
      .pipe(
        map((res: HttpResponse<any>) => Number.parseInt(res.headers.get('Location'))),
        tap(id=>console.log(id))
      )
  }
  deletePact(port: number): Observable<any>{
    return this.http
      .delete(`${environment.apihost}/server/${port}`)
      .pipe(tap(()=>this.getAllPacts().subscribe()));
  }

  updatePact(port: number, server: MockServer){
    return this.http
      .put(`${environment.apihost}/server/${port}`, server.pact)
      .pipe(tap(()=>console.log('Pact updated')))
  }
}

export class MockServer {
  constructor(
    public port: number,
    public name: string = '',
    public pact: string = ''
  ) {
  }
}
