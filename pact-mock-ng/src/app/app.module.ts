import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {RouterModule} from "@angular/router";
import {AddPactComponent} from './add-pact/add-pact.component';
import {PactMasterComponent} from './pact-master/pact-master.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {PactService} from "./pact.service";
import {MatButtonModule, MatExpansionModule, MatIconModule, MatToolbarModule} from "@angular/material";
import {AceEditorModule} from "ng2-ace-editor";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";


@NgModule({
  declarations: [
    AppComponent,
    AddPactComponent,
    PactMasterComponent,
  ],
  imports: [
    HttpClientModule, FormsModule,
    BrowserModule, RouterModule.forRoot([
      {path: 'add', component: AddPactComponent},
      {path: 'pacts', component: PactMasterComponent},
      {path: '', redirectTo: '/pacts', pathMatch: 'full'},
    ]),
    MatToolbarModule, MatButtonModule, AceEditorModule, MatExpansionModule, BrowserAnimationsModule, MatIconModule
  ],
  providers: [PactService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
