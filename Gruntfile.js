module.exports = function (grunt) {
    grunt.initConfig({
        exec: {
            "build": 'tsc -p tsconfig.json',
            "buildng": 'cd pact-mock-ng && npm run build',
            "run": 'node dist/index.js',
            "runng": 'cd pact-mock-ng && npm start',
        }
    });
    grunt.loadNpmTasks('grunt-exec');

    grunt.registerTask('build', ['exec:build']);
    grunt.registerTask('buildng', ['exec:buildng']);
    grunt.registerTask('run', ['exec:run']);
    grunt.registerTask('runng', ['exec:runng']);

    grunt.registerTask('', ['build', 'run']);
}
