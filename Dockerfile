FROM node:8
MAINTAINER Daniel Richter
WORKDIR /pact
EXPOSE 8080
COPY node_modules/ node_modules/
COPY index.js index.js
CMD ["node","index.js"]